var express         = require('express');
var path            = require('path');
var favicon         = require('serve-favicon');
var logger          = require('morgan');
var cookieParser    = require('cookie-parser');
var bodyParser      = require('body-parser');
var partials        = require('express-partials');
var constants       = require('./config/constants');
var functions       = require('./config/functions');

// routes
var index           = require('./routes/index');
var contacts        = require('./routes/contacts');
var history         = require('./routes/history');
var send_message    = require('./routes/send_message');
var app             = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// load the express-partials middleware
app.use(partials());

// configurations
app.use(function (req, res, next) {
    // req locals
    res.locals.constants = constants;
    res.locals.functions = functions;
    next();
});

// routes declaration
app.use('/', index);
app.use('/contacts', contacts);
app.use('/send-message', send_message);
app.use('/history', history);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
