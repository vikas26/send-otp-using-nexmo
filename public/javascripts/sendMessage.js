// send message
var sendMessage = function (data, callback_fn) {
    showLoader('Sending OTP');
    $.ajax({
        type: 'POST',
        data: data,
        url: '/send-message/send-now/',
        cache: false,
        dataType: 'application/json'
    }).always(function (response) {
        hideLoader();
        callback_fn(data, response);
    });
}

// send message callback
var sendMessageCallback = function (data, response) {
    var json_response = JSON.parse(response.responseText);
    var toastedMessage = json_response.error != true ? 'Success : ' : 'Error : ';
    toastedMessage += json_response.message;
    var toastColor = json_response.error != true ? 'teal' : 'red';
    Materialize.toast(toastedMessage, 4000, toastColor, function(){
        // console.log('message');
    });
}

// send message click button
var sendMessageClickBtn = function () {
    var data = {
        'id': document.getElementById('id').value,
        'to': document.getElementById('mobile').value,
        'message': document.getElementById('otp_message').value
    };
    sendMessage(data, sendMessageCallback);
}