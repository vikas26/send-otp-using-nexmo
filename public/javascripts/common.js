// show loader
var showLoader = function (text) {
    $("#loader .loader-text").text(text);
    $("#loader").removeClass('hide').show();
}

// hide loader
var hideLoader = function () {
    $("#loader").hide();
}