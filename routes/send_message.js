var express         = require('express');
var partials        = require('express-partials')
var router          = express.Router();
var sendMsgModel    = require('../models/sendMessage.js');
var messageModel    = require('../models/message.js');

/* POST send message */
router.post('/send-now/', function(req, res, next) {

    // json response to request
    var json_response = {
        error: false,
        message: '',
        inputs: ''
    };
    
    // inout validations
    if (req.body.to == undefined) {
        json_response.error = true;
        json_response.message = 'Mobile number not specified';
        res.send(json_response);
    } else if (req.body.message == undefined) {
        json_response.error = true;
        json_response.message = 'Message not specified';
        res.send(json_response);
    }

    // inputs
    var input = {
        id: req.body.id,
        to: req.body.to,
        message: req.body.message
    };

    // inputs
    json_response.inputs = input;

    // return response
    var returnResponse = function () {
        res.send(json_response);
    }

    // sent message response
    var sentMessageResponse = function (err, message) {
        if (message != undefined && message.sid != undefined) {
            json_response.message = 'Message successfully sent';

            // insert into message table
            messageModel.insertNewMessage(input, returnResponse);
        } else {
            json_response.error = true;
            json_response.message = err.message;
            returnResponse();
        }
    };

    // send message
    sendMsgModel.sendMessage(input, sentMessageResponse);
});

module.exports = router;
