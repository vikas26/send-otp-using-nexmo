var express         = require('express');
var router          = express.Router();

/* GET redirect to product listing */
router.get('/', function(req, res, next) {
    res.redirect('/contacts');
});

module.exports = router;
