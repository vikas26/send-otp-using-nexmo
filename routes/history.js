var express         = require('express');
var router          = express.Router();
var messageModel    = require('../models/message.js');

/* GET sent messages listing. */
router.get('/', function(req, res, next) {
    var data = {
        title: 'Kisan network | Vikas',
        page: 'history',
        limit: 10
    };

    // page not defined
    if (req.query.pageNo == undefined){
        data.pageNo = 1;
    } else {
        data.pageNo = parseInt(req.query.pageNo);
        data.pageNo = data.pageNo < 1 ? 1 : data.pageNo;
    }

    // count sent messages
    var countSentMessages = function(totalMessages) {
        data.totalMessages = totalMessages;
        data.totalPages = Math.ceil(totalMessages / data.limit);
        // get sent messages
        messageModel.getSentMessages(data.pageNo, data.limit, sentMessages);
    };

    // sent messages
    var sentMessages = function(response) {
        data.messages = response;
        renderData();   // render page
    };

    // render data
    var renderData = function() {
        // history messages
        res.render('history/history', data);
    };

    // get sent messages
    messageModel.getSentMessagesCount(countSentMessages);
});

module.exports = router;
