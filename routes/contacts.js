var express         = require('express');
var partials        = require('express-partials')
var router          = express.Router();
var contactsModel   = require('../models/contact.js');

/* GET contacts list */
router.get('/', function(req, res, next) {
    var data = {
        title: 'Kisan network | Vikas',
        page: 'contacts-list'
    };

    // append fettched contacts
    var appendContacts = function(response) {
        data.contacts = response;
        renderData();
    };

    // render data
    var renderData = function() {
        // render contacts
        res.render('contacts/contacts', data);
    };

    // get all contacts
    contactsModel.getAllContacts(appendContacts);
});

/* GET contact details */
router.get('/contact-detail/:id', function(req, res, next) {
    var data = {
        id: req.params.id,
        title: 'Kisan network | Vikas',
        page: 'contact-detail'
    };

    // attach contact details
    var contactDetails = function(response) {
        data.details = response;
        renderData();
    };

    // render data
    var renderData = function() {
        // render contacts
        res.render('contacts/contact-detail', data);
    };

    // get contact details by id
    contactsModel.getSpecificContactById(data.id, contactDetails);
});

/* GET compose */
router.get('/compose/:id', function(req, res, next) {
    var data = {
        id: req.params.id,
        title: 'Kisan network | Vikas',
        page: 'compose'
    };

    // gemerate random number between
    var getRandomArbitrary = function(min, max) {
        return Math.round(Math.random() * (max - min) + min);
    }

    // attach contact details
    var contactDetails = function(response) {
        data.details = response;
        // generate random number
        data.otp = getRandomArbitrary(100000, 999999);
        // render page
        renderData();
    };

    // render data
    var renderData = function() {
        // render contacts
        res.render('contacts/compose', data);
    };

    // get contact details by id
    contactsModel.getSpecificContactById(data.id, contactDetails);
});

module.exports = router;
