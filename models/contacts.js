var db          = require('../config/db.js');
var mysql       = require('mysql');
var connection  = mysql.createConnection (db.database);

// get all contacts
// called from contacts.js - 
//        /
// returns json - all contacts
exports.getAllContacts = function (callback_fn) {

    var query = 'SELECT * from contacts';
    connection.query(query, function (err, rows) {
        var response = JSON.parse(JSON.stringify(rows));
        callback_fn(response);
    });
};

// get contact details from id
// called from contacts.js - 
//        /contact-detail/:id
//        /compose/:id
// returns json - specific contact
exports.getSpecificContactById = function (id, callback_fn) {

    var query = 'SELECT * from contacts where id=' + id + ' limit 1';
    connection.query(query, function (err, rows) {
        var response = JSON.parse(JSON.stringify(rows[0]));
        callback_fn(response);
    });
};