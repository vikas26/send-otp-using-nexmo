var db          = require('../config/db.js');
var mysql       = require('mysql');
var connection  = mysql.createConnection (db.database);

// get all sent messages
// called from history.js - 
//        /
// returns json - all sent messages
exports.getSentMessages = function (page, limit, callback_fn) {

    var query = `SELECT Message.*, Contact.* from 
                    messages as Message 
                    INNER JOIN 
                    contacts as Contact 
                    on Message.sent_to = Contact.id
                    order by Message.sent_at desc
                    limit ${limit} offset ${(page - 1) * limit}
                    `;
    connection.query(query, function (err, rows) {
        var response = JSON.parse(JSON.stringify(rows));
        callback_fn(response);
    });
};

// get contact details from id
// called from send-message.js - 
//        /send-now
// returns json
exports.insertNewMessage = function (inputs, callback_fn) {
    var currentTS = new Date();

    var query = `insert into messages (message_text, sent_to) values ("${ inputs.message }", "${ inputs.id }")`;
    connection.query(query, function (err, result) {
        if (err) console.log('error in inserting row!');
        callback_fn();
    });
};

// get all sent messages
// called from history.js - 
//        /
// returns json - sent messages count
exports.getSentMessagesCount = function (callback_fn) {

    var query = `SELECT count(Message.id) as totalMessages from 
                    messages as Message`;
    connection.query(query, function (err, rows) {
        var response = JSON.parse(JSON.stringify(rows[0]));
        callback_fn(response.totalMessages);
    });
};
