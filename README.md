# A SIMPLE CONTACTS APP (WITH OTP SMS SENDING FUNCTIONALITY) #

- This is a 24 hour test done for a firm

Overview - This web app sends an OTP (via SMS) to a list of contacts, one at a time. It is coded using NodeJS and ExpressJS. 

1. The main page has two tabs
	- First Tab
		a. List of contacts
			- List of contacts stored in db.
			- Upon selecting a contact from the list, the contact info page for that contact is displayed (in a popup).
		b. Contact info page : Contact Details with button
			- Display contact data: name, phone number.
			- Button saying “Send Message” which on click opens next screen.
		c. New Message Screen Compose
			- Components: Text Field, Send button
			- Text Field needs to have text along the following lines: “Hi. Your OTP is: 123456”. 123456 needs to be a random six-digit number.
			- Upon hitting send, the contents of the text field will be sent to the selected recipient as an SMS. For this Nexmo service is used for sending SMS.
	- Second Tab: List of messages sent
		- It shows list of messages already sent in descending order of date-time
		- Each list item shows the name of the contact, time and OTP sent in the SMS
