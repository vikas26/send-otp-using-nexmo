// convert all one digits to two digits
exports.twoDigits = function(d) {
    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}

// js to mysql date
exports.dateToMySqlDate = function (date) {
    return date.getUTCFullYear() + "-" + 
            exports.twoDigits(1 + date.getUTCMonth()) + "-" + 
            exports.twoDigits(date.getUTCDate()) + " " + 
            exports.twoDigits(date.getUTCHours()) + ":" + 
            exports.twoDigits(date.getUTCMinutes()) + ":" + 
            exports.twoDigits(date.getUTCSeconds());
}