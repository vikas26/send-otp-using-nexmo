// production database credentials
var production = {
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'kisan_net_prod'
};

// local database credentials
var local = {
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'kisan_net_local'
};

// mode
module.exports.mode = 'local'; // (local, prod)

// connect
module.exports.database = module.exports.mode == 'local' ? local : production;
